app.config(['$stateProvider', function ($stateProvider) {
    $stateProvider
        .state('industry', {
            url: '/industry',
            controller: 'IndustryCtrl',
            controllerAs: 'industry',
            templateUrl: './views/industry/industry.html'
        });
}])