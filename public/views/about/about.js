app.config(['$stateProvider', function ($stateProvider) {
    $stateProvider
        .state('about', {
            url: '/about',
            controller: 'AboutCtrl',
            controllerAs: 'about',
            templateUrl: './views/about/about.html'
        });
}])