app.config(['$stateProvider', function ($stateProvider) {
    $stateProvider
        .state('service', {
            url: '/service',
            controller: 'ServiceCtrl',
            templateUrl: './views/service/service.html'
        })
        .state('service.index', {
            url: '/',
            controller: 'ServiceCtrl',
            templateUrl: './views/service/service_index.html'
        })
        .state('service.detail', {
            url: '/:id',
            controller: 'ServiceCtrl',
            templateUrl: './views/service/service_detail.html'
        });
}]);