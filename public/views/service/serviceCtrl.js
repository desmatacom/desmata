app.controller('ServiceCtrl', ['$scope', '$rootScope', function ($scope, $rootScope) {
    $rootScope.partnerShow = true;
    $scope.service = {
        title: 'Services Page',
        services: [
                    {
                        title: 'Data Analytics',
                        icon: 'fa-file-text-o',
                        description: 'lor bla bla text bla bla textbla bla textbla bla textbla bla textbla bla textbla bla textbla bla textbla bla textbla bla text'
                    },
                    {
                        title: 'Cloud Services',
                        icon: 'fa-cloud',
                        description: 'lor bla bla text bla bla textbla bla textbla bla textbla bla textbla bla textbla bla textbla bla textbla bla textbla bla textbla bla textbla bla textbla bla textbla bla text'
                    },
                    {
                        title: 'MsSql Server Solutions',
                        icon: 'fa-server',
                        description: 'lor bla bla text bla bla textbla bla textbla bla textbla bla textbla bla textbla bla textbla bla textbla bla textbla bla text'
                    },
                    {
                        title: 'Quality Assurance',
                        icon: 'fa-heart-o',
                        description: 'lor bla bla text bla bla textbla bla textbla bla textbla bla textbla bla textbla bla textbla bla textbla bla textbla bla text'
                    },
                    {
                        title: 'JAVA/j2ee',
                        icon: 'fa-coffee',
                        description: 'lor bla bla text bla bla textbla bla textbla bla textbla bla textbla bla textbla bla textbla bla textbla bla textbla bla text'
                    },
                    {
                        title: 'Business Analysis',
                        icon: 'fa-cog',
                        description: 'lor bla bla text bla bla textbla bla textbla bla textbla bla textbla bla textbla bla textbla bla textbla bla textbla bla text'
                    },
                    {
                        title: 'Project Managers',
                        icon: 'fa-user',
                        description: 'lor bla bla text bla bla textbla bla textbla bla textbla bla textbla bla textbla bla textbla bla textbla bla textbla bla text'
                    },
                    {
                        title: 'System Administrators',
                        icon: 'fa-long-arrow-right',
                        description: 'lor bla bla text bla bla textbla bla textbla bla textbla bla textbla bla textbla bla textbla bla textbla bla textbla bla text'
                    }
                ]
    };
}]);