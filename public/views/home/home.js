app.config(['$stateProvider', function ($stateProvider) {
    $stateProvider
        .state('home', {
            url: '/',
            controller: 'HomeCtrl',
            controllerAs: 'home',
            templateUrl: './views/home/home.html'
            // template: "<h1>Hello world</h1>"
        });
}]);