app.controller('HomeCtrl', ['$scope', '$rootScope', function ($scope, $rootScope) {
    $rootScope.partnerShow = true;
    $scope.title = "Welcome to Nepasit ";

    $scope.service = {
        title: 'Services Page',
        services: [
            {
                title: 'Training Programs',
                icon: 'fa-square-o',
                description: 'lor bla bla text bla bla textbla bla textbla bla textbla bla textbla bla textbla bla textbla bla textbla bla textbla bla text'
            },
            {
                title: 'IT Services',
                icon: 'fa-commenting-o',
                description: 'lor bla bla text bla bla textbla bla textbla bla textbla bla textbla bla textbla bla textbla bla textbla bla textbla bla text'
            },
            {
                title: 'Business Inteligence',
                icon: 'fa-comment-o',
                description: 'lor bla bla text bla bla textbla bla textbla bla textbla bla textbla bla textbla bla textbla bla textbla bla textbla bla text'
            },
            {
                title: 'IT Services',
                icon: 'fa-heart-o',
                description: 'lor bla bla text bla bla textbla bla textbla bla textbla bla textbla bla textbla bla textbla bla textbla bla textbla bla text'
            }
        ]
    };
}]);