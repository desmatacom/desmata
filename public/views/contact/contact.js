app.config(['$stateProvider', function ($stateProvider) {
    $stateProvider
        .state('contact', {
            url: '/contact',
            controller: 'ContactCtrl',
            controllerAs: 'contact',
            templateUrl: './views/contact/contact.html'
        });
}]);